const addBtn = document.getElementById('content-submit');
const withdrawBtn = document.getElementById('content-submit-withdraw');
const btn3 = document.getElementById('logout');
let datavalue;
let accountnumber;
const collectDataAdd= () => {
    accountnumber = document.getElementById('account-no').textContent;
    datavalue = {
        name: document.getElementById('content-card').value,
        amount: document.getElementById('content-amount').value
    }

}

const collectDataWithdraw = () => {
    accountnumber = document.getElementById('account-no').textContent;
    datavalue = {
        name: document.getElementById('transaction-name').value,
        amount: document.getElementById('content-amount-withdraw').value
    }

}

//const accountnumber = document.getElementById('account-no').value;
console.log(accountnumber);
addBtn.addEventListener('click', function(){
    collectDataAdd();
    const datasubmit = axios({
        method: 'POST',
        url: `/add/${accountnumber}`,

        data:{
            datavalue,
            raisedby: 'zainul'
        }

    }).then((e)=>{
        console.log(`data ${e.data}`)
        location.assign(`/dashboard/${e.data._id}`)
    }).catch((e) => {console.log(e)})
})
withdrawBtn.addEventListener('click', async function(){
    collectDataWithdraw();
    const datasubmit = await axios({
        method: 'POST',
        url: `/withdraw/${accountnumber}`,

        data:{
            datavalue,
            raisedby: 'zainul'
        }

    }).then((e)=>{
        console.log(`data ${e.data}`)
        location.assign(`/dashboard/${e.data._id}`)
    }).catch((e) => {console.log(e)})
})

logout = async () => {
    try {
      const res = await axios({
        method: 'GET',
        url: '/logout'
      });
      if ((res.data.status = 'success')) location.reload(true);
    } catch (err) {
      console.log(err.response);
      alert('error', 'Error logging out! Try again.');
    }
  };
  
  btn3.addEventListener('click', logout);