const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');
const Transaction = require('./transactionModel');
const userSchema = new mongoose.Schema({
    name:String,
    mobile:Number,
    pan: String,
    accountnumber: String,
    email: String,
    dob: {
        type:Number,
        set: (val) => {
            return val.replaceAll('-','')
        }
    },
    password: String,
    transactions:[{
        type:mongoose.Schema.ObjectId,
        ref:Transaction
    }],
    balance:{
      type: Number,
      default:30
      
    }

});

// userSchema.static('count',function (){
//     this.count++;
// }

userSchema.pre('save', async function (next){
    const listuser = await User.find().count();
    this.accountnumber = `RBI`+ String(listuser).padStart(4,0);
    next();
})

userSchema.pre('save', async function(next) {
    // Only run this function if password was actually modified
    //if (!this.isModified('password')) return next();
  
    // Hash the password with cost of 12
    this.password = await bcrypt.hash(this.password, 12);
 // bcryptjs
    // Delete passwordConfirm field
    
    next();
  });
  

userSchema.pre(/^find/, function(next){
   this.populate('transactions');
   next();
})

userSchema.methods.correctPassword = async function(
    candidatePassword,
    userPassword
  ) {
    return await bcrypt.compare(candidatePassword, userPassword);
  };
  

const User = mongoose.model('User', userSchema);
module.exports = User;
