const express = require('express');
const app = express();
const path = require('path');
const dotenv = require('dotenv');
const User = require('./models/userModel');
const Transaction = require('./models/transactionModel');
const mongoose = require('mongoose');
const req = require('express/lib/request');
const cookieParser = require('cookie-parser');
const jwt = require('jsonwebtoken');
const { promisify } = require('util');
const catchAsync = require('./utils/catchAsync');

dotenv.config({ path: './config.env' });
//Zainul%40123
const DB = `mongodb+srv://zainul1998:${process.env.dbPassword}@robinhoodtest.wqzvy.mongodb.net/Robinhood?retryWrites=true&w=majority`;
let port = process.env.PORT ||3000;
mongoose
  .connect(DB, {
    useNewUrlParser: true,
    useUnifiedTopology: true,    
  })
  .then(() => console.log('DB connection successful!'));
app.listen(port, () => {console.log('server started')});


app.use(express.static(path.join(__dirname,'public')));
app.use(express.json({limit:'10Kb'}));
app.use(express.urlencoded({extended:true,limit:'10Kb'}));
app.use(cookieParser());
app.set('view engine','pug');
app.set('views', path.join(__dirname,'views'));




// Refactor the code below
// create user send user creted successfully then redirect to dashboard page
// for login as first show login success fully then redirect to dashboard page


const register = catchAsync(async(req,res, next) => {
  console.log(req.body)
    const userentry =  req.body.datavalue || new Object(req.body);
    const newUser = await User.create(userentry);
       //res.send(`hello from server side ` )
       req.body.userid = newUser._id;
       res.locals.userid = newUser._id;
       req.params.userid = newUser._id;
       
       if(newUser != null){
         console.log('user registered');
         createSendToken(newUser,201,req,res);
         res.redirect(`/dashboard/${req.body.userid}`);


       }
       else{
         res.send('unable to register user')
       }

   
  });


const isLoggedIn = catchAsync(async (req, res, next) => {
    if (req.cookies.jwt) {
      console.log(req.cookies);
      try {
        // 1) verify token
        const decoded = await promisify(jwt.verify)(
          req.cookies.jwt,
          'high-five-secret'
        );
        
         

         //jwt.verify(req.cookies.jwt, 'high-five-secret',(_ , deco) => console.log(deco));
        // console.log(decoded);
  
        // 2) Check if user still exists
        const currentUser = await User.findById(decoded.id);
        if (!currentUser) {
          // code to relogin
          res.redirect('/login');

        }
  
        // 3) Check if user changed password after the token was issued
        // if (currentUser.changedPasswordAfter(decoded.iat)) {
        //   return next();
        // }
  
        // THERE IS A LOGGED IN USER
        res.locals.user = currentUser;
        //console.log(currentUser)
        return next();
      } catch (err) {
        console.log('no jwt exists');
        res.redirect('/login');
      }
    }
    else{
      
      res.redirect('/login');
    }
    //next();
  });
  
  const dashboardshow = async (req, res,next)=>{
    let userdetails;
    if (res.locals.user._id == req.params.id) {
      userdetails = res.locals.user;}
    else{

      userdetails = await User.findById(req.body.userid || '61c8b62632565b52b664c886');
    }
  res.status(200).render('dashboard', {userdetails});
    
}



// const verifyUser = async () => {
//   const user = await User.findOne({username: req.body.username, password: req.body.password}) ;
//   if(!user){
//     res.send('no user')
//   }
//   req.body.data = {id: user._id};
//   next()
// }






  const signToken = id => {
    return jwt.sign({ id }, 'high-five-secret', {
      expiresIn: 60*60
    });
    
  };
  
  const createSendToken = (user, statusCode, req, res) => {
    const token = signToken(user._id);
  
    res.cookie('jwt', token, {
      expires: new Date(
        Date.now() + 3 * 24 * 60 * 60 * 1000
      ),
      // httpOnly: true,
      // secure: req.secure || req.headers['x-forwarded-proto'] === 'https'
    });
  
    // Remove password from output
    user.password = undefined;
  
    // res.status(statusCode).json({
    //   status: 'success',
    //   token,
    //   data: {
    //     user
    //   }
    // });
  };
  

const login = catchAsync(async (req,res,next) => {
   //if not logged in state then
   console.log(req.body)
   const { accno, password } = req.body;
     // 1) Check if email and password exist
  if (!accno || !password) {
    //return next(new AppError('Please provide email and password!', 400));
    return res.send('invalid email or password')
  }
  // 2) Check if user exists && password is correct
  const user = await User.findOne({ accountnumber:accno }).select('+password');

  if (!user || !(await user.correctPassword(password, user.password))) {
    //return next(new AppError('Incorrect email or password', 401));
    return res.send('invalid email or password');
  }

  // 3) If everything ok, send token to client
  createSendToken(user, 200, req, res);


   //const user = await User.findOne({accountnumber: req.body.accno, password:req.body.password})
   //show login screen
   //after login render the dashbaoard page
   //as earlier withdrawal form and addition form.
   console.log(user)
   req.body.userid = user._id;
   //res.send('hello')
   res.redirect(`/dashboard/${req.body.userid}`);
   next()
});

app.get('/', (req,res, next)=>{
  res.status(200).render('home');
  });
  
app.route('/register').post(register).get((req,res,next) => {
    res.status(200).render('registration')
  })



app.route('/login').post(login).get((req,res,next) => {
  res.status(200).render('registration')
});

app.route('/dashboard/:id?').get(isLoggedIn, dashboardshow);

app.post('/add/:accountnumber', catchAsync(async(req,res, next) => {
  console.log(req.body);
  console.log(req.params);

  if(!req.body.datavalue.name || req.body.datavalue.name == '') next(err);
  const newtransactionentry ={
    name: `Added Using card ${req.body.datavalue.name}`,
    amount: req.body.datavalue.amount
  } ;
  const newTransaction = await Transaction.create(newtransactionentry);
  console.log(newTransaction);
  const userdata = await User.findOne({accountnumber: req.params.accountnumber});
  const array = userdata.transactions;
  // array.push(newTransaction._id);
  const updateduser = await User.findOneAndUpdate({accountnumber: req.params.accountnumber}, {
    $push: {transactions: newTransaction._id},
    $inc:{balance:newTransaction.amount}
      
  })
  res.send(updateduser);
  //req.body.userid = updateduser._id;
  //next()
}))



app.post('/withdraw/:accountnumber', catchAsync(async(req,res, next) => {
  console.log(req.body);
  console.log(req.params);
  const newtransactionentry = req.body.datavalue;
  const newTransaction = await Transaction.create(newtransactionentry);
  console.log(newTransaction);
  const userdata = await User.findOne({accountnumber: req.params.accountnumber});
  const array = userdata.transactions;
  // array.push(newTransaction._id);
  const updateduser = await User.findOneAndUpdate({accountnumber: req.params.accountnumber}, {
    $push: {transactions: newTransaction._id},
    $inc:{balance:-newTransaction.amount}
    
  
  
  })
  res.send(updateduser);
  }))






app.route('/logout').get((req, res) => {
  res.cookie('jwt', 'loggedout', {
    expires: new Date(Date.now() + 10 * 1000),
    httpOnly: true
  });
  res.status(200).json({ status: 'success' });
});


app.use((err,req,res,next) => {

  res.send('error occured please check');
})